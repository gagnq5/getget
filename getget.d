import std.stdio, std.string, std.algorithm, std.file, std.array, std.conv, std.process, std.range;

enum MINERFILES = `\\minerfiles\dfs\software\itwindist\`;

void main(string[] args) {
  ////
  //Parse command line args for packages and uninstall option
  ////
  auto wantedPaks = args[1..$];
  bool uninstall = false, searchVista = false, searchXP = false, nosrc = false, updates = false;
  string installString = "i";
  string extraOptions = " ";

  for (int x = 0; x < wantedPaks.length; x++) {
    if (wantedPaks[x] == "-u") {
      if (!uninstall) {
        uninstall = true;
        installString = "uni";
        extraOptions ~= "--uninstall ";
      }
      wantedPaks = wantedPaks.remove(x);
    }
    else if (wantedPaks[x] == "-vista") {
      searchVista = true;
      wantedPaks = wantedPaks.remove(x);
    }
    else if (wantedPaks[x] == "-winxp") {
      searchXP = true;
      wantedPaks = wantedPaks.remove(x);
    }
    else if (wantedPaks[x] == "-nosrc") {
      if (!nosrc) {
        nosrc = true;
        extraOptions ~= "--no-source-files ";
      }
      wantedPaks = wantedPaks.remove(x);
    }
    else if (wantedPaks[x] == "-updates") {
      if (!updates) {
        updates = true;
        extraOptions ~= "--updates ";
      }
      wantedPaks = wantedPaks.remove(x);
    }
  }

  if (wantedPaks.length == 0) {
    writeln("Usage: getget package1 package2:subpackage ...");
    writeln("Extra commands:");
    writeln("  -u        | Passes --uninstall to all scripts");
    writeln("  -winxp    | Additionally searches the winxp folder for packages");
    writeln("  -vista    | Additionally searches the vista folder for packages");
    writeln("  -nosrc    | Passes --no-source-files to all scripts");
    writeln("  -updates  | Passes --updates to all scripts");
    return;
  }

  ////
  //Make sure K: drive there
  //// 

  //if (!exists(MINERFILES ~ "")) {
  //  writeln("It appears your K:\\ drive isn't mounted. Do that before running getget.");
  //  writeln("The network path to mount is \"\\\\minerfiles.mst.edu\\dfs\\software\\itwindist\".");
  //  return;
  //}

  ////
  //Search for viable packages for the given args
  ////

  auto packageList = chain(dirEntries(MINERFILES ~ `win10\appdist`, SpanMode.shallow),
                           dirEntries(MINERFILES ~ `win8\appdist`, SpanMode.shallow),
                           dirEntries(MINERFILES ~ `win7\appdist`, SpanMode.shallow))
                     .filter!(a => a.isDir).array;

  string[] installList = [];

  if (searchVista) {
    packageList ~= dirEntries(MINERFILES ~ `vista\appdist`, SpanMode.shallow).filter!(a => a.isDir).array;
  }
  if (searchXP) {
    packageList ~= dirEntries(MINERFILES ~ `winxp\appdist`, SpanMode.shallow).filter!(a => a.isDir).array;
  }


  foreach (x; wantedPaks) {
    string packageToAdd = searchForPackage(x.toLower, packageList);
    if (packageToAdd != "") installList ~= packageToAdd;
  }

  ////
  //Confirm they really want to do this
  ////

  if (installList.length == 0) {
    writeln("\n==========\n\n",
            "Looks like there aren't any packages to install... Aborting.");
    return;
  }

  writeln("\n==========\n");
  writefln("Packages to %snstall:", installString);
  foreach (item; installList) {
    writeln("  ", item);
  }
  write("\nIs this okay? (y/n): ");

  if (getBooleanResponse()) {
    writefln("%snstalling packages now...", installString.capitalize);
  }
  else {
    writefln("Aborting %snstallation.", installString);
    return;
  }

  ////
  //Begin installation
  ////

  string[] failed = [];

  foreach (item; installList) {
    string scriptToRun = item ~ `\prod\update.pl`;

    if (!exists(scriptToRun)) {
      if (exists(item ~ `\dev\update.pl`)) {
        write("It appears only a dev package is available. Is this okay? : ");
        
        if (getBooleanResponse()) {
          scriptToRun = item ~ `\dev\update.pl`;
        }     
        else {
          writeln("Skipping this package.");
          failed ~= item;
          continue;
        }
      }
      else {
        writeln("ERROR: It appears no update.pl exists for this package, meaning it's broken or invalid. Skipping.");
        failed ~= item;
        continue;
      }
    }

    writeln("\n==========\n\n",
            "[[[Running script for ", item[item.lastIndexOf(`\n`)+1..$],"]]]\n");

    auto cmd = pipeShell(scriptToRun ~ extraOptions, Redirect.stdout | Redirect.stderrToStdout);
    bool alreadyAddedToFailed = false;

    static immutable ERROR_MSGS = ["phase unsuccessful", "has already been installed", "INDETERMINATE", "compilation errors"];

    foreach (line; cmd.stdout.byLine) {
      if (!alreadyAddedToFailed) {
        if (ERROR_MSGS.any!(x => line.canFind(x))) {
          failed ~= item;
          alreadyAddedToFailed = true;
        }
      }
      writeln(line); 
    }

    wait(cmd.pid);

    //if the return code was 0 or the app was reported to already be installed, it failed
    /*if (cmd.status != 0 || (!uninstall && cmd.output.canFind(" has already been installed."))) {
      failed ~= item;
    }*/

    //writeln(cmd.output, "\n");
  }

  ////
  //Give list of failed packages if necessary, otherwise say goodbye
  ////

  writeln("\n==========\n");
  
  if (failed.length != 0) {
    writefln("Looks like a few packages failed to %snstall:", installString);
    foreach (x; failed) {
      writeln("  ", x);
    }
    writeln("Any packages not listed here have finished successfully.");
  }
  else {
    writeln(installString.capitalize, "nstallation finished successfully!");
  }
}

/**
 * Recursively searches for a package in a list of directories.
 * 
 * Note: A subpackage name consisting of a space means that getget decided it needed to look ahead one directory level
 * because the directory chosen was a folder containing subpackages instead of a package itself.
 */
string searchForPackage(string wanted, ref DirEntry[] list, bool isSub = false) {
  string result;
  string subString = isSub ? "sub" : "";

  string chosenSub = null;
  auto splitUp = findSplit(wanted, ":");
  if (!splitUp[1].empty) {
    chosenSub = splitUp[2];
    wanted = splitUp[0];
  }

  auto listCopy = list.filter!(a => a.name.toLower.canFind(wanted)).array;

  if (listCopy.length == 0) {
    writefln("No %spackages were found for given name \"%s\". Skipping.", subString, wanted);
    return "";
  }
  else if (listCopy.length == 1) {
    writefln("Closest match found for \"%s\": %s", wanted, listCopy[0].name);
    result = listCopy[0].name;
    
    if (!(exists(result ~ `\prod\update.pl`) || exists(result ~ `\dev\update.pl`)))
      chosenSub = ": ";
  }
  else { 
    //multiple packages are available, give them a choice
    if (chosenSub == " ") writeln("Choose a subpackage:");
    else writefln("Candidates for \"%s\":", wanted);
    
    foreach (c; 0..listCopy.length) {
      string folder = listCopy[c].name[MINERFILES.length..listCopy[c].name.lastIndexOf(`\appdist`)];
      string packageName = listCopy[c].name[listCopy[c].lastIndexOf(`\`)+1..$];
      
      writefln("  (%d)\t%s (%s)", c+1, packageName, folder);
    }
    write("Choice: ");

    int choice = getNumberedResponse(cast(int) listCopy.length) - 1;

    result = listCopy[choice].name;

    if (exists(result ~ `\prod\update.pl`) || exists(result ~ `\dev\update.pl`)) {
      writefln("Choosing %spackage %s", subString, result);
    }
    else {
      if (chosenSub is null) chosenSub = ": ";
    }
  }


  if (chosenSub !is null && chosenSub != " ") {
    auto placeToSearch = dirEntries(result, SpanMode.shallow)
                          .filter!(a => a.isDir && !a.name.endsWith(".git"))
                          .array;
    return searchForPackage(chosenSub, placeToSearch, true);
  }
  else {
    return result;
  }
}

/**
 * Properly asks for a "yes" or "no" response. Case-insensitive, accepts "yes", "no", "y", "n", "true", "false"
 * Writes "Come again? : " if the input was invalid.
 */
bool getBooleanResponse() {
  int decision = 0;

  while (decision == 0) { 
    string response = readln.strip.toLower;
    if (response == "yes" || response == "y" || response == "true") {
      decision = 1;
    }
    else if (response == "no" || response == "n" || response == "false") {
      decision = -1;
    }
    if (decision == 0) write("Come again? : ");
  }

  return (decision == 1);
}

/**
 * Properly for a number from 1 to a given number numChoices, inclusive
 * Writes "Come again? : " if the input was invalid.
 */
int getNumberedResponse(int numChoices) {
  int choice = -1; 
  while (choice < 1 || choice > numChoices) {
    bool bad = false;

    try {
      choice = readln.strip.to!int;
    }
    catch (Exception e) { bad = true; }

    if (bad || choice < 1 || choice > numChoices)
      write("Come again? : ");
  }
  return choice;
}